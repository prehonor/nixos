{ stdenv, lib, fetchgit, cmake, meson, ninja, pkg-config, wayland, git, libdbusmenu-gtk3
, alsa-lib, gtkmm3, gtk-layer-shell, pulseaudio, wayfire, wf-config, wlroots
}:
let
  source = {
    stable = {
      version = "0.8.0";
      rev = "c9639087aca3ad69bbd8f56f4213768639b4c8d0";
      sha256 = "sha256-eCga6ZdxqJYKc9yAI77fZUXOSaee8ijCE0XiJRJtDAg=";
    };
    master = {
      version = "0.8.0";
      rev = "892ce5cb19bac966da937b45766d4cdcad40f401"; # a7504215430db88baa503ca22328ca4d16dadf24
      sha256 = "sha256-zEF41+ibtiriIUgqJI8Ro7CzLuUWE8cL9fRRuqUaUsE="; # sha256-4HrPhCcrbKY13Oj/WRkDJ//Tj5Srqi8eWTEV2NQPqjg=
    };
  };
in
stdenv.mkDerivation rec {

  pname = "wf-shell";
  inherit (source.master) version;

  src = fetchgit { 
    url = "https://gitee.com/github-10784632_admin_admin/${pname}.git";
    inherit (source.master) rev sha256;

    fetchSubmodules = true;
  };
  dontUseCmakeConfigure = true;
  nativeBuildInputs = [ cmake meson ninja pkg-config wayland ];
  buildInputs = [
    alsa-lib gtkmm3 gtk-layer-shell pulseaudio wayfire wf-config wlroots libdbusmenu-gtk3 
  ];

  mesonFlags = [ "--sysconfdir" "/etc" ];
  
  PKG_CONFIG_WAYFIRE_LIBDIR = "lib";
  PKG_CONFIG_WAYFIRE_METADATADIR = "share/wayfire/metadata";

  meta = with lib; {
    homepage = "https://github.com/WayfireWM/wf-shell";
    description = "GTK3-based panel for Wayfire";
    license = licenses.mit;
    maintainers = with maintainers; [ qyliss wucke13 ];
    platforms = platforms.unix;
  };
}
