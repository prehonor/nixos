{ stdenv, lib, fetchgit, cmake, meson, ninja, pkg-config, wayland, wrapGAppsHook, libxkbcommon
, gtk3, gtkmm3, libevdev, libepoxy, libxml2, wayfire, wayland-protocols, wf-config, wf-shell, wlroots
}:
let
  source = {
    stable = {
      version = "0.7.5";
      rev = "70e1bdc29769b83515eee25d9ad4e3b294369c33"; # 70e1bdc29769b83515eee25d9ad4e3b294369c33 b725da3cc4587c8f3b25a7a699849fe94ced6e5f;
      sha256 = "sha256-IdsXYCJUO/poAUcox5E/NcUjKhKk3itmfP/CzRPBFmE=="; # sha256-IdsXYCJUO/poAUcox5E/NcUjKhKk3itmfP/CzRPBFmE= sha256-LJR9JGl49o4O6LARofz3jOeAqseGcmzVhMnhk/aobUU;
    };
    master = {
      version = "0.8.0";
      rev = "9f871a8e2b189cabb50da2130386937097e713a3"; # eee4d05addfdd987ade2e052435345fb739c4bba
      sha256 = "sha256-h/GYCY3AoV24CBK+l0qR87+O+ziuzHO6Bgjh8YmS1Zk="; # sha256-nCSQP/FSWtvjoMD007hH5zRZ8XeQvamCs/DZryBS64o=
    };
  };
in
stdenv.mkDerivation rec {

  pname = "wcm";
  inherit (source.master) version;
  
  src = fetchgit {
    url = "https://gitee.com/github-10784632_admin_admin/${pname}.git";
    inherit (source.master) rev sha256;
    fetchSubmodules = true;
  };
  dontUseCmakeConfigure = true;
  nativeBuildInputs = [ cmake meson ninja pkg-config wayland wrapGAppsHook ];
  buildInputs = [
    gtk3 gtkmm3 libevdev libxml2 wayfire wayland libepoxy
    wayland-protocols wf-config wf-shell wlroots libxkbcommon
  ];

  meta = with lib; {
    homepage = "https://github.com/WayfireWM/wcm";
    description = "Wayfire Config Manager";
    license = licenses.mit;
    maintainers = with maintainers; [ qyliss wucke13 ];
    platforms = platforms.unix;
  };
}
