{ stdenv, lib, fetchgit, cmake, meson, ninja, pkg-config
, doctest, glm, libevdev, libxml2, cairo, pango, libdrm, libinput, libxkbcommon, wayland, wayland-protocols
}:
let
  source = {
    stable = {
      version = "0.7.1";
      rev = "62e3897f207f49b1a3bbb85ba4b11d9fea239ec1";
      sha256 = "sha256-ADUBvDJcPYEB9ZvaFIgTfemo1WYwiWgCWX/z2yrEPtA=";
    };
    master = {
      version = "0.8.0";
      rev = "af1bddc9d7191b9902edcb4c74572eac65577806"; # 3da1c2254e645ba139b0db268349eb00d65162a6
      sha256 = "sha256-9CwHCBjtbjdZZEWoc1M5tv2osBz+ZxVwi/L/iAwuPY0="; # sha256-BKx+R/895vT3Kf2YNc8xajDp/8KI3isANp1nbFEz4RM=
    };
  };
in
stdenv.mkDerivation rec {

  pname = "wf-config";
  inherit (source.master) version;

  src = fetchgit {
    url = "https://gitee.com/github-10784632_admin_admin/${pname}.git";
    inherit (source.master) rev sha256;
  };
  
  nativeBuildInputs = [ cmake meson ninja pkg-config wayland-protocols ];
  buildInputs = [ doctest libevdev libxml2 wayland cairo pango libdrm libinput libxkbcommon ];
  propagatedBuildInputs = [ glm ];

  # CMake is just used for finding doctest.
  dontUseCmakeConfigure = true;

  doCheck = true;

  meta = with lib; {
    homepage = "https://github.com/WayfireWM/wf-config";
    description = "Library for managing configuration files, written for Wayfire";
    license = licenses.mit;
    maintainers = with maintainers; [ qyliss wucke13 ];
    platforms = platforms.unix;
  };
}
