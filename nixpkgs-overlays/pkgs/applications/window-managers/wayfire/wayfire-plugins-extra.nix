{
  cairo,
  fetchgit,
  glibmm,
  glm,
  cmake,
  libxkbcommon,
  xcbutilwm,
  meson,
  ninja,
  pango,
  systemd,
  libinput,
  libevdev,
  pkg-config,
  stdenv,
  wf-config,
  wlroots,
  wayfire,
  wayland,
  wayland-protocols,
  libGL
}:
let
  source = {
    stable = {
      version = "0.7.5";
      rev = "b698f613763d02c30cdc642b35d6e8e98252b936";
      sha256 = "sha256-hnsRwIrl0+pRKhRlrF/Wdlu6HkzLfYukGk4Hzx3wNeo=";
    };

    master = {
      version = "0.8.0";
      rev = "7d94a21c7b59e400e47555c3d3dac1dac4694933"; # 5386f5219eb2071dfd255836d91ef953d779c47f
      sha256 = "sha256-49+s9XwvekaEdCAzHlu8ALiRbV2Tdqplul/aeDfCfDg="; # sha256-EO/7c2oMUVfrfw3Cgdld0KNnuTpZbZqkMXk73OXQQQM=
    };
  };
in
stdenv.mkDerivation rec {

  # 包含子模块，但是由于项目默认没有使用子模块所以暂时用fetchgit以备万一 
  pname = "wayfire-plugins-extra";
  inherit (source.master) version;
  
  src = fetchgit {
    url = "https://gitee.com/github-10784632_admin_admin/${pname}.git";
    inherit (source.master) rev sha256;
  };
  /* patches = [
    ./event-patch.diff
  ]; */

  dontUseCmakeConfigure = true;

  nativeBuildInputs = [
    meson
    ninja
    pkg-config
    wayland
    cmake
  ];

  buildInputs = [
    cairo
    glibmm
    libevdev
    libinput
    systemd
    glm
    libxkbcommon
    xcbutilwm
    pango
    wlroots
    wf-config
    wayfire
    wayland
    wayland-protocols
    libGL
  ];

  PKG_CONFIG_WAYFIRE_LIBDIR = "lib";
  PKG_CONFIG_WAYFIRE_METADATADIR = "share/wayfire/metadata";
}
