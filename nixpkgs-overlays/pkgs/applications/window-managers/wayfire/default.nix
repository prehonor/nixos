{ lib, stdenv, fetchgit, cmake, meson, ninja, pkg-config
, wayland, wayland-protocols, wf-config, wlroots, mesa
, cairo, pango, vulkan-headers, vulkan-loader, xorg, xwayland
, doctest, libdrm, libexecinfo, libinput, libjpeg, libxkbcommon, glslang, xcbutilerrors, xcbutilwm, seatd
, libevdev, nlohmann_json
# , glm, freetype, pixman, libcap , libuuid, libxml2, libpng
, libxml2 ,libGL
}:
let
  source = {
    stable = {
      version = "0.7.5";
      rev = "207b2b3a2e5ab8187622e9ccccfb7779fd4bf5b9"; # 6057d363cceeba25a55ca4c7cb665d468e5dae03    207b2b3a2e5ab8187622e9ccccfb7779fd4bf5b9
      sha256 = "sha256-uoaUb7OZSKa4EyraD08vD/6fU3P9dvRvjZjDkoNTvKI="; # sha256-YijuFryfAaSDHGc2IpIzmqW4hrrUU5NUAzf8JPLU13c=   sha256-uoaUb7OZSKa4EyraD08vD/6fU3P9dvRvjZjDkoNTvKI=; 
    };

    master = {
      version = "0.8.0";
      rev = "bfce47612dab198ec1cf4c50bcfafc3ba9832cbc";  #997afd493bd6de8d073491efa5cad132b10ffb15
      sha256 = "sha256-9BMa43phk1DoKjb4s/DC0NCO/hy+BIosdwXOMkcN2/4="; #sha256-SjQ+/lGzH+DykgW+zwgV9hpD+lVmhDfxWy8oaKZdboQ=
    };

  };
in
stdenv.mkDerivation rec {

  pname = "wayfire";
  inherit (source.master) version;
  
  
  src = fetchgit {
    url = "https://gitee.com/github-10784632_admin_admin/${pname}.git";
    
    inherit (source.master) rev sha256;
    
    fetchSubmodules = true;

  };
  /* patches = [
    # https://github.com/WayfireWM/wayfire/commit/2daec9bc30920c995700252b4915bbc2839aa1a3#diff-fff9797dc434bcdbb9cb1f1cb46f3a4f6de611034a0eaefe53dd0882b1095778
    # https://github.com/WayfireWM/wayfire/commit/883dacf8fe1eec5463269755879dfc71b481e7c9
    # https://github.com/WayfireWM/wayfire/commit/2daec9bc30920c995700252b4915bbc2839aa1a3
    ./upgrade-wlroots.diff
  ]; */

  nativeBuildInputs = [ cmake meson ninja pkg-config wayland ];
  buildInputs = [
    cairo doctest libdrm libexecinfo libinput libjpeg libxkbcommon wayland
    wayland-protocols wf-config wlroots mesa pango
    vulkan-headers vulkan-loader xwayland xorg.xcbutilrenderutil 
    glslang xcbutilerrors seatd xcbutilwm
    libxml2# libuuid libxml2 libpng
    libevdev nlohmann_json # glm  freetype pixman libcap 
    libGL
  ];

  # CMake is just used for finding doctest.
  dontUseCmakeConfigure = true;

  mesonFlags = [ "--sysconfdir" "/etc" 
    "-Duse_system_wlroots=enabled"
    "-Duse_system_wfconfig=enabled" 
    ];

  meta = with lib; {
    homepage = "https://wayfire.org/";
    description = "3D Wayland compositor";
    license = licenses.mit;
    maintainers = with maintainers; [ qyliss wucke13 ];
    platforms = platforms.unix;
  };
}
