{
  lib,
  stdenv,
  fetchurl,
  dpkg,
  wrapGAppsHook3,
  autoPatchelfHook,
  udev,
  libdrm,
  libpqxx,
  xcb-util-cursor,
  unixODBC,
  gst_all_1,
  xorg,
  libpulseaudio,
  mysql80,
}:

stdenv.mkDerivation rec {
  pname = "freedownloadmanager";
  version = "6.22";

  src = fetchurl {
    url = "https://files2.freedownloadmanager.org/6/latest/freedownloadmanager.deb";
    hash = "sha256-G6kS3lHHudW33rVZbWIV2m/lpA0IIOPWWz1OCuhl8/o=";
  };

  unpackPhase = "dpkg-deb -x $src .";

  nativeBuildInputs = [
    dpkg
    wrapGAppsHook3
    autoPatchelfHook
  ];

  buildInputs =
    [
      libdrm
      libpqxx
      unixODBC
      stdenv.cc.cc
      mysql80
    ]
    ++ (with gst_all_1; [
      gstreamer
      gst-libav
      gst-plugins-base
      gst-plugins-good
      gst-plugins-bad
      gst-plugins-ugly
    ])
    ++ (with xorg; [
      xcbutilwm # libxcb-icccm.so.4
      xcb-util-cursor # libxcb-cursor.so.0
      xcbutilimage # libxcb-image.so.0
      xcbutilkeysyms # libxcb-keysyms.so.1
      xcbutilrenderutil # libxcb-render-util.so.0
      libpulseaudio
    ]);

  runtimeDependencies = [
    (lib.getLib udev)
  ];

  installPhase = ''
    mkdir -p $out/bin
    cp -r opt/freedownloadmanager $out
    cp -r usr/share $out
    ln -s $out/freedownloadmanager/fdm $out/bin/${pname}
    rm -r $out/freedownloadmanager/plugins/sqldrivers/libqsqlmimer.so
    substituteInPlace $out/share/applications/freedownloadmanager.desktop \
      --replace 'Exec=/opt/freedownloadmanager/fdm' 'Exec=${pname}' \
      --replace "Icon=/opt/freedownloadmanager/icon.png" "Icon=$out/freedownloadmanager/icon.png"
  '';

  meta = with lib; {
    description = "A smart and fast internet download manager";
    homepage = "https://www.freedownloadmanager.org";
    license = licenses.unfree;
    platforms = [ "x86_64-linux" ];
    sourceProvenance = with sourceTypes; [ binaryNativeCode ];
    maintainers = with maintainers; [ ];
  };
}
