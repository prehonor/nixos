{ stdenv, symlinkJoin,fetchFromGitHub, clang, idris2-with-api, ... }:

stdenv.mkDerivation rec {
  pname = "idris2-lsp";
  version = "0.6.0";

  src = fetchFromGitHub {
    owner = "idris-community";
    repo = "idris2-lsp";
    rev = "db9c02d13d4665bdae4af0990ea1ab971ad31799"; # 526902b3a2e8e4f203df2cdd9ed26cf5556eeab4

    # Do not fetch the bundled version of Idris 2
    fetchSubmodules = false;

    sha256 = "sha256-pLh5qBKoi1phvoiaCbpgxs38xaXM1VfD5lQI70I5F38="; # sha256-pLh5qBKoi1phvoiaCbpgxs38xaXM1VfD5lQI70I5F38=
  };
  buildInputs = [ idris2-with-api ];

  doCheck = false;

  makeFlags = [
    "VERSION_TAG=${version}"
    "PREFIX=$(out)"
  ];

  installTargets = [ "install-only" ];
}