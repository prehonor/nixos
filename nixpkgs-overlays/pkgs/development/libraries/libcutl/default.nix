{ lib, stdenv
, build2
, fetchurl
, xercesc
}:
stdenv.mkDerivation rec {
  pname = "libcutl"; 
  version = "1.11.0-b.9+1"; #1.11.0-b.8 1.11.0-b.9+1

  outputs = [ "out" "dev" "doc" ];

  majmin = builtins.head ( builtins.match "([[:digit:]]+).*" "${version}" );
    src = fetchurl {
      url = "https://pkg.cppget.org/${majmin}/beta/${pname}/${pname}-${version}.tar.gz";
      sha256 = "sha256-GY443axh8jNGhlCenZcme3bnbf3mw3ic3RNSTzHxFHY="; #sha256-v0BtncxetxuMKVham6SA8LwJImss93ow+a0rvldILeI= sha256-GY443axh8jNGhlCenZcme3bnbf3mw3ic3RNSTzHxFHY=
    };

  nativeBuildInputs = [
    build2
  ];
  buildInputs = [
    xercesc
  ];
  enableParallelBuilding = true;

  meta = with lib; {
    description = "libcutl";
    longDescription = ''

      http://www.codesynthesis.com/products/libcutl/

    '';
    homepage = "https://www.codesynthesis.com/products/libcutl/";
    changelog = "https://git.codesynthesis.com/cgit/libcutl/libcutl/tree/NEWS";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ r-burns ];
    platforms = platforms.all;
  };
}