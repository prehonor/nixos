{ lib, stdenv
, build2
, libodb
, boost_x
, fetchurl
}:
stdenv.mkDerivation rec {
  pname = "libodb-boost";
  version = "2.5.0-b.25";

  outputs = [ "out" "dev" "doc" ];

  src = fetchurl {
    url = "https://pkg.cppget.org/1/beta/odb/libodb-boost-${version}.tar.gz";
    hash = "sha256-ymwIm2pLVV+ZYQBH0SuF40GXRXBxFGvxipo67M51Ch0=";
  };

  nativeBuildInputs = [
    build2
  ];
  buildInputs = [
    libodb 
  ];
  propagatedBuildInputs = [
     boost_x
  ];

  doCheck = true;

  meta = with lib; {
    description = "libodb-boost";
    longDescription = ''
      libodb-boost
    '';
    homepage = "https://www.codesynthesis.com/products/odb/";
    changelog = "https://git.codesynthesis.com/cgit/odb/libodb-boost/tree/NEWS";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ r-burns ];
    platforms = platforms.all;
  };
}