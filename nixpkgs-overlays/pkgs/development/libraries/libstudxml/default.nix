{ lib, stdenv
, build2
, fetchurl
}:
stdenv.mkDerivation rec {
  pname = "libstudxml";
  version = "1.1.0-b.10+2";

  outputs = [ "out" "dev" "doc" ];

  src = fetchurl {
    url = "https://pkg.cppget.org/1/beta/libstudxml/libstudxml-${version}.tar.gz";
    hash = "sha256-Lyzat4HacLpsIU9zk3FFqG9Tok9V0nbN0lY7bYifwuQ=";
  };

  nativeBuildInputs = [
    build2
  ];

  meta = with lib; {
    description = "A streaming XML pull parser and streaming XML serializer implementation for modern, standard C++";
    homepage = "https://www.codesynthesis.com/projects/libstudxml/";
    license = lib.licenses.mit;
    maintainers = with lib.maintainers; [ tomasajt ];
    platforms = lib.platforms.all;
  };
}