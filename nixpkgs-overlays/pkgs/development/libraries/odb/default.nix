{ lib, stdenv
, build2
, fetchurl
, gmp
, libcutl
, libstudxml
, enableShared ? !stdenv.hostPlatform.isStatic
, enableStatic ? !enableShared
}:
stdenv.mkDerivation rec {
  pname = "odb";
  version = "2.5.0-b.25+1";

  outputs = [ "out" "dev" "doc" ];

  src = fetchurl {
    url = "https://pkg.cppget.org/1/beta/odb/odb-${version}.tar.gz";
    hash = "sha256-JLjTo8rw7Kg6XDJWzj3xiofcUaLJqaY/PVpGhDj3m7A=";
  };

  nativeBuildInputs = [
    build2
  ];
  buildInputs = [
    libcutl libstudxml gmp
  ];


  build2ConfigureFlags = [
    "config.bin.lib=${build2.configSharedStatic enableShared enableStatic}"
  ];

  doCheck = true;

  meta = with lib; {
    description = "odb";
    longDescription = ''
       ODB compiler
      http://www.codesynthesis.com/products/odb/

    '';
    homepage = "https://www.codesynthesis.com/products/odb/";
    changelog = "https://git.codesynthesis.com/cgit/odb/odb/tree/NEWS";
    license = licenses.gpl2Only;
    maintainers = with maintainers; [ r-burns ];
    platforms = platforms.all;
  };
}