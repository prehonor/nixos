{ stdenv
, fetchurl
, lib
, autoPatchelfHook
, wrapQtAppsHook
, gmpxx
, gnustep
, libbsd
, libffi_3_3
, ncurses6
, dpkg
}:

stdenv.mkDerivation rec {
  pname = "hopper";
  version = "5.7.7";
  rev = "v4";

  src = fetchurl {
    url = "https://prehonor-generic.pkg.coding.net/download/back/hopper-${rev}_${version}_amd64.deb";
    hash = "sha256-zh9K6/Ysz3DBaLo0Btx4jAdvWuKqC7cxZ0sXrUK2ssU=";
  };

  nativeBuildInputs = [
    autoPatchelfHook
    wrapQtAppsHook
    dpkg
  ];
  unpackPhase = "true";
  buildInputs = [
    gnustep.libobjc
    libbsd
    libffi_3_3
    ncurses6
  ];

  installPhase = ''
    runHook preInstall
    mkdir -p $out/bin
    mkdir -p $out/lib
    mkdir -p $out/share
    dpkg -x $src $out
    cp $out/opt/hopper-${rev}/bin/Hopper $out/bin/hopper
    cp \
      --archive \
      $out/opt/hopper-${rev}/lib/libBlocksRuntime.so* \
      $out/opt/hopper-${rev}/lib/libdispatch.so* \
      $out/opt/hopper-${rev}/lib/libgnustep-base.so* \
      $out/opt/hopper-${rev}/lib/libHopperCore.so* \
      $out/opt/hopper-${rev}/lib/libkqueue.so* \
      $out/opt/hopper-${rev}/lib/libobjcxx.so* \
      $out/opt/hopper-${rev}/lib/libpthread_workqueue.so* \
      $out/lib
    cp -r $out/usr/share $out
    rm -rf $out/opt
    runHook postInstall
  '';

  postFixup = ''
    substituteInPlace "$out/share/applications/hopper-${rev}.desktop" \
      --replace "Exec=/opt/hopper-${rev}/bin/Hopper" "Exec=$out/bin/hopper"
  '';

  meta = with lib; {
    homepage = "https://www.hopperapp.com/index.html";
    description = "A macOS and Linux Disassembler";
    license = licenses.unfree;
    maintainers = with maintainers; [
      luis
      Enteee
    ];
    platforms = platforms.linux;
  };
}