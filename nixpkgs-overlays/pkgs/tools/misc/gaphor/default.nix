{ lib
, babel
, better-exceptions
, buildPythonApplication
, copyDesktopItems
, defusedxml
, dulwich
, fetchPypi
, gaphas
, generic
, gobject-introspection
, gtk4
, gtksourceview5
, jedi
, libadwaita
, librsvg
, makeDesktopItem
, pango
, pillow
, poetry-core
, pycairo
, pydot
, pygobject3
, python
, tinycss2
, wrapGAppsHook4
}:

buildPythonApplication rec {
  pname = "gaphor";
  version = "2.26.0";

  format = "pyproject";

  src = fetchPypi {
    inherit pname version;
    sha256 = "sha256-e0K5bfgPqlJh8qrAz40c/w3ANzkfa/6txuqzQDJYXfE=";
  };

  nativeBuildInputs = [
    copyDesktopItems
    gobject-introspection
    poetry-core
    wrapGAppsHook4
  ];

  buildInputs = [
    gtksourceview5
    pango
  ];

  propagatedBuildInputs = [
    babel
    better-exceptions
    defusedxml
    dulwich
    gaphas
    generic
    jedi
    libadwaita
    pillow
    pycairo
    pydot
    pygobject3
    tinycss2
  ];

  desktopItems = makeDesktopItem {
    name = pname;
    exec = "gaphor";
    icon = "gaphor";
    comment = meta.description;
    desktopName = "Gaphor";
  };

  # Disable automatic wrapGAppsHook4 to prevent double wrapping
  dontWrapGApps = true;

  postInstall = ''
    install -Dm644 $out/${python.sitePackages}/gaphor/ui/icons/hicolor/scalable/apps/org.gaphor.Gaphor.svg $out/share/pixmaps/gaphor.svg
  '';

  preFixup = ''
    makeWrapperArgs+=(
      "''${gappsWrapperArgs[@]}" \
      --prefix XDG_DATA_DIRS : "${gtk4}/share/gsettings-schemas/${gtk4.name}/" \
      --set GDK_PIXBUF_MODULE_FILE "${librsvg.out}/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache"
    )
  '';

  meta = with lib; {
    description = "Simple modeling tool written in Python";
    maintainers = [ ];
    homepage = "https://github.com/gaphor/gaphor";
    license = licenses.asl20;
    platforms = [ "x86_64-linux" ];
  };
}