{
  description = "Prehonor NixOS";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; # "github:NixOS/nixpkgs/nixos-24.05" github:nixos/nixpkgs/nixos-unstable;
    nur.url = "github:nix-community/NUR";
    zen-browser = {
      url = "github:Axot017/zen-browser-flake";
      # url = "github:youwen5/zen-browser-flake"; # "myamusashi/zen-twilight-flake"
      inputs.nixpkgs.follows = "nixpkgs";
    };
    # nixos-cosmic = {
    #   url = "github:lilyinstarlight/nixos-cosmic";
    #   inputs.nixpkgs.follows = "nixpkgs";
    # };
    # hyprland.url = "github:hyprwm/Hyprland";
    # nixos-hardware.url = github:NixOS/nixos-hardware/master;
    # emacs-overlay.url = "github:nix-community/emacs-overlay/7ba0cd76c4e5cbd4a8ac7387c8ad2493caa800f0";

    home-manager = {
      url = "github:nix-community/home-manager"; # "github:nix-community/home-manager/release-24.05" "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };
  outputs =
    {
      self,
      nixpkgs,
      home-manager,
      nur,
      flake-compat,
      zen-browser,
    }@attrs: # , nixos-cosmic
    let
      system = "x86_64-linux";
      hm-module = [
        home-manager.nixosModules.home-manager
        {
          home-manager.useGlobalPkgs = true;
          home-manager.useUserPackages = true;
          home-manager.users.prehonor = import /home/prehonor/.config/nixpkgs/home.nix;

          # Optionally, use home-manager.extraSpecialArgs to pass arguments to home.nix

          # home-manager.users.gdm = {
          #   home.stateVersion = "24.05";
          #   home.file.".config/monitors.xml".text = ''
          #     <monitors version="2">
          #       <configuration>
          #         <logicalmonitor>
          #           <x>1920</x>
          #           <y>0</y>
          #           <scale>1</scale>
          #           <monitor>
          #             <monitorspec>
          #               <connector>DVI-D-1</connector>
          #               <vendor>GAM</vendor>
          #               <product>GM185</product>
          #               <serial>0x00000000</serial>
          #             </monitorspec>
          #             <mode>
          #               <width>1920</width>
          #               <height>1080</height>
          #               <rate>60.000</rate>
          #             </mode>
          #           </monitor>
          #         </logicalmonitor>
          #         <logicalmonitor>
          #           <x>0</x>
          #           <y>0</y>
          #           <scale>1</scale>
          #           <primary>yes</primary>
          #           <monitor>
          #             <monitorspec>
          #               <connector>HDMI-1</connector>
          #               <vendor>VSC</vendor>
          #               <product>VX2363 Series</product>
          #               <serial>U0G155000607</serial>
          #             </monitorspec>
          #             <mode>
          #               <width>1920</width>
          #               <height>1080</height>
          #               <rate>60.000</rate>
          #             </mode>
          #           </monitor>
          #         </logicalmonitor>
          #       </configuration>
          #     </monitors>
          #   '';
          # };
        }
      ];
    in
    {
      nixosConfigurations = {
        prehonor = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = attrs // {
            inputs = self.inputs;
          }; # // { inputs = self.inputs; }
          modules = [
            ./configuration.nix
            # hyprland.nixosModules.default
            # {programs.hyprland.enable = true;}
            # {
            #     services.desktopManager.cosmic.enable = true;
            #     services.displayManager.cosmic-greeter.enable = true;
            # }
            # nixos-cosmic.nixosModules.default
          ] ++ hm-module;
        };
      };
      /*
        homeConfigurations = (
          import /home/prehonor/.config/nixpkgs/home.nix {
            inherit system home-manager;
            nixpkgs = prehonor.nixpkgs;
          }
        );
      */
    };
}
